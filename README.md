# AirDroid

Android app to display air quality

## Setup

Create a new secret.properties file based into the secret.properties.default.
To create a new AirQuality api key go [here](https://aqicn.org/data-platform/token/#/)

## Functionalities

### v3.0.0
* Allow to change city for a small set, update the widgets
### v2.0.0
* Allow to create a widget with Santiago, Chile air quality
### v1.0.0
* Display air quality for Santiago, Chile

## License

[World Air Quality Index](https://waqi.info/)