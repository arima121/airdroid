package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class Measurements {
    @SerializedName("pm25")
    public PM25 pm25;
    @SerializedName("pm10")
    public PM10 pm10;
    @SerializedName("no2")
    public NO2 no2;
    @SerializedName("co")
    public CO co;
    @SerializedName("o3")
    public O3 o3;
    @SerializedName("t")
    public Temp temp;
    @SerializedName("p")
    public Pressure pressure;
    @SerializedName("w")
    public Wind wind;
    @SerializedName("h")
    public Humidity humidity;
}
