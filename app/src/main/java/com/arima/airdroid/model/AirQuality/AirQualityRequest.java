package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class AirQualityRequest {
    @SerializedName("token")
    public String token;
    @SerializedName("city")
    public String city;
}
