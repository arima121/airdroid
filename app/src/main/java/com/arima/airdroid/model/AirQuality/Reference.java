package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class Reference {
    @SerializedName("url")
    public String url;
    @SerializedName("name")
    public String name;
}
