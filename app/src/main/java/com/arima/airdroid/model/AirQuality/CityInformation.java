package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class CityInformation {
    @SerializedName("name")
    public String name;
    @SerializedName("geo")
    public String[] latLon;
    @SerializedName("url")
    public String url;
}
