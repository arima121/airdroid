package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class StationData {
    @SerializedName("idx")
    public long id;
    @SerializedName("aqi")
    public long airQuality;
    @SerializedName("time")
    public TimeInformation timeInformation;
    @SerializedName("city")
    public CityInformation cityInformation;
    @SerializedName("attributions")
    public Reference[] references;
    @SerializedName("iaqi")
    public Measurements measurements;
}
