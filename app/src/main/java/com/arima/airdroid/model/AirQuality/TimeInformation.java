package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class TimeInformation {
    @SerializedName("s")
    public String localTime;
    @SerializedName("tz")
    public String timeZone;
    @SerializedName("v")
    public long timestamp;
}
