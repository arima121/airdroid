package com.arima.airdroid.model;

public class Option {
    String flag;
    String name;
    String displayName;

    public Option(String flag, String name, String displayName) {
        this.flag = flag;
        this.name = name;
        this.displayName = displayName;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
