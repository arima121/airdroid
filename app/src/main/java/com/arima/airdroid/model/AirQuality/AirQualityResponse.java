package com.arima.airdroid.model.AirQuality;

import com.google.gson.annotations.SerializedName;

public class AirQualityResponse {
    @SerializedName("status")
    public String status;
    @SerializedName("data")
    public StationData data;
}
