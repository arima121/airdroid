package com.arima.airdroid;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arima.airdroid.model.AirQuality.AirQualityResponse;
import com.google.gson.Gson;

import static android.appwidget.AppWidgetManager.ACTION_APPWIDGET_UPDATE;

/**
 * Implementation of App Widget functionality.
 */
public class Widget extends AppWidgetProvider {

    public static final String KEY ="b77784b0-d12b-43b8-b306-9648f6ba2026";

    static void updateAppWidget(final Context context, final AppWidgetManager appWidgetManager, final int appWidgetId) {
        final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format(Constant.API_URL, getCountryPreference(context), BuildConfig.AIR_QUALITY_API_KEY);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override public void onResponse(String response) {
                        Gson parser = new Gson();
                        AirQualityResponse objectResponse = parser.fromJson(response, AirQualityResponse.class);

                        views.setTextViewText(R.id.appwidget_text, Long.toString(objectResponse.data.airQuality));
                        int backgroundColor = MainActivity.colorValue(objectResponse.data.airQuality);
                        views.setTextViewText(R.id.messageText, context.getResources().getText(MainActivity.messageResource(backgroundColor)));
                        appWidgetManager.updateAppWidget(appWidgetId, views);
                    }
                },
                new Response.ErrorListener() {
                    @Override public void onErrorResponse(VolleyError error) {
                        views.setTextViewText(R.id.appwidget_text, "0");
                    }
                });
        queue.add(stringRequest);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(KEY)) {
            int[] ids = intent.getExtras().getIntArray(KEY);
            this.onUpdate(context, AppWidgetManager.getInstance(context), ids);
        } else {
            super.onReceive(context, intent);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public static String getCountryPreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        return preferences.getString("city", "santiago");
    }
}

