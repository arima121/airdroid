package com.arima.airdroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import com.arima.airdroid.adapter.SelectionAdapter;

public class SelectionActivity extends AppCompatActivity {
    RecyclerView list;
    SelectionActivity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        thisActivity = this;

        list = findViewById(R.id.list);
        list.setHasFixedSize(true);

        final SelectionAdapter selectionAdapter = new SelectionAdapter(Constant.OPTIONS);

        selectionAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCity(thisActivity, Constant.OPTIONS[list.getChildAdapterPosition(v)].getName());
                updateMyWidgets(thisActivity);
                finish();
            }
        });

        list.setAdapter(selectionAdapter);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
    }

    public static void saveCity(ContextWrapper context, String value) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("city", value);
        editor.commit();
    }

    public static void updateMyWidgets(Context context) {
        AppWidgetManager man = AppWidgetManager.getInstance(context);
        int[] ids = man.getAppWidgetIds(new ComponentName(context,Widget.class));
        Intent updateIntent = new Intent();
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(Widget.KEY, ids);
        context.sendBroadcast(updateIntent);
    }
}
