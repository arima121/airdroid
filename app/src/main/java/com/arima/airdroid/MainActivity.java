package com.arima.airdroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arima.airdroid.model.AirQuality.AirQualityResponse;
import com.arima.airdroid.model.Option;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    TextView airQualityValue;
    TextView messageValue;
    TextView countryView;
    TextView pm25View;
    TextView pm10View;
    TextView o3View;
    TextView coView;
    TextView no2View;
    TextView tempView;
    TextView pressureView;
    TextView humidityView;
    TextView windView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        airQualityValue = findViewById(R.id.air_condition);
        messageValue = findViewById(R.id.message);
        countryView = findViewById(R.id.country);
        pm25View = findViewById(R.id.pm25);
        pm10View = findViewById(R.id.pm10);
        o3View = findViewById(R.id.o3);
        coView = findViewById(R.id.co);
        no2View = findViewById(R.id.no2);
        tempView = findViewById(R.id.temp);
        pressureView = findViewById(R.id.pressure);
        humidityView = findViewById(R.id.humidity);
        windView = findViewById(R.id.wind);

        refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    public void refresh() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = String.format(Constant.API_URL, getCountryPreference(this), BuildConfig.AIR_QUALITY_API_KEY);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override public void onResponse(String response) {
                        setAirCondition(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override public void onErrorResponse(VolleyError error) {
                        airQualityValue.setText("That didn't work!");
                    }
                });
        queue.add(stringRequest);
    }

    public void setAirCondition(String apiResponse) {
        Gson parser = new Gson();
        AirQualityResponse response = parser.fromJson(apiResponse, AirQualityResponse.class);
        airQualityValue.setText(Long.toString(response.data.airQuality));
        int backgroundColor = colorValue(response.data.airQuality);
        messageValue.setText(getString(messageResource(backgroundColor)));
        messageValue.setTextColor(getResources().getColor(textColorValue(backgroundColor)));
        messageValue.setBackgroundColor(getResources().getColor(backgroundColor));
        airQualityValue.setBackgroundColor(getResources().getColor(backgroundColor));
        airQualityValue.setTextColor(getResources().getColor(textColorValue(backgroundColor)));


        String empty = getResources().getString(R.string.PlaceholderEmptyResponse);
        String nanoUnit = getResources().getString(R.string.nanoUnit);
        String tempUnit = getResources().getString(R.string.tempUnit);
        String windUnit = getResources().getString(R.string.windUnit);
        String pressureUnit = getResources().getString(R.string.pressureUnit);
        String humidityUnit = getResources().getString(R.string.humidityUnit);

        countryView.setText(response.data.cityInformation.name);
        pm25View.setText(response.data.measurements.pm25 == null? empty : response.data.measurements.pm25.value + " " + nanoUnit);
        pm10View.setText(response.data.measurements.pm10 == null? empty : response.data.measurements.pm10.value + " " + nanoUnit);
        no2View.setText(response.data.measurements.no2 == null? empty : response.data.measurements.no2.value + " " + nanoUnit);
        o3View.setText(response.data.measurements.o3 == null? empty : response.data.measurements.o3.value + " " + nanoUnit);
        coView.setText(response.data.measurements.co == null? empty : response.data.measurements.co.value + " " + nanoUnit);
        tempView.setText(response.data.measurements.temp == null? empty : response.data.measurements.temp.value + tempUnit);
        windView.setText(response.data.measurements.wind == null? empty : response.data.measurements.wind.value + " " + windUnit);
        pressureView.setText(response.data.measurements.pressure == null? empty : response.data.measurements.pressure.value + " " + pressureUnit);
        humidityView.setText(response.data.measurements.humidity == null? empty : response.data.measurements.humidity.value + humidityUnit);
    }

    public static int colorValue(long value) {
        if(value <= 50) {
            return R.color.good;
        } else if(value > 50 && value <= 100) {
            return R.color.moderate;
        } else if(value > 100 && value <= 150) {
            return R.color.unhealthySensitive;
        } else if(value > 150 && value <= 200) {
            return R.color.unhealthy;
        } else if(value > 200 && value <= 300) {
            return R.color.veryUnhealthy;
        } else {
            return R.color.hazardous;
        }
    }

    public static int textColorValue(int backgroundColor) {
        switch (backgroundColor) {
            case R.color.good:
            case R.color.unhealthy:
            case R.color.veryUnhealthy:
            case R.color.hazardous:
                return R.color.whiteText;
            default:
                return R.color.blackText;
        }
    }

    public static int messageResource(int backgroundColor) {
        switch (backgroundColor) {
            case R.color.good:
                return R.string.goodMessage;
            case R.color.moderate:
                return R.string.moderateMessage;
            case R.color.unhealthySensitive:
                return R.string.unhealthySensitiveMessage;
            case R.color.unhealthy:
                return R.string.unhealthyMessage;
            case R.color.veryUnhealthy:
                return R.string.veryUnhealthyMessage;
            case R.color.hazardous:
                return R.string.hazardousMessage;
            default:
                return R.string.PlaceholderEmptyResponse;
        }
    }

    public void goToSelectionList(View view) {
        Intent intent = new Intent(this, SelectionActivity.class);
        startActivity(intent);
    }

    public static String getCountryPreference(ContextWrapper context) {
        SharedPreferences preferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        return preferences.getString("city", "santiago");
    }
}
