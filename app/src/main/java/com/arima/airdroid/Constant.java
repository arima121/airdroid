package com.arima.airdroid;

import com.arima.airdroid.model.Option;

public class Constant {
    public static final String API_URL = "https://api.waqi.info/feed/%s/?token=%s";
    public static final Option[] OPTIONS= new Option[]{
            new Option("\uD83C\uDDE8\uD83C\uDDF1", "santiago", "Santiago"),
            new Option("\uD83C\uDDFA\uD83C\uDDF8", "newyork", "New York"),
            new Option("\uD83C\uDDE8\uD83C\uDDF3", "shangai", "Shangai"),
            new Option("\uD83C\uDDEE\uD83C\uDDF3", "bangalore", "Bengaluru"),
    };
}
