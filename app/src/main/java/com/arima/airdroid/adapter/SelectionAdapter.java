package com.arima.airdroid.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arima.airdroid.R;
import com.arima.airdroid.model.Option;

public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.SelectionViewHolder> implements View.OnClickListener {
    private Option[] options;
    private View.OnClickListener listener;

    public SelectionAdapter (Option[] options) {
        this.options = options;
    }

    @NonNull
    @Override
    public SelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.selection_list_item, parent, false);
        itemView.setOnClickListener(this);
        SelectionViewHolder viewHolder = new SelectionViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SelectionViewHolder holder, int position) {
        Option item = options[position];
        holder.setSelection(item);
    }

    @Override
    public int getItemCount() {
        return options.length;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

    public static class SelectionViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView flag;

        public SelectionViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            flag = itemView.findViewById(R.id.flag);
        }
        public void setSelection(Option option) {
            name.setText(option.getDisplayName());
            flag.setText(option.getFlag());
        }
    }
}
